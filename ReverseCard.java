public class ReverseCard extends ColorCards{
    

    public ReverseCard(char color){
        super(color);
    }
    
    public boolean canPlay(Cards card){
            if(card instanceof NumberedCards){
                NumberedCards NumCard = ((NumberedCards)card);
                if(this.getColor() == NumCard.getColor() || this.getColor() == 'w') {return true;}
                else{return false;}
    
            }else if(card instanceof PickUp2Card){
                PickUp2Card CardPickUp = ((PickUp2Card)card);
                if(this.getColor() == CardPickUp.getColor() || this.getColor() == 'w') {return true;}
                else{return false;}
    
            }else if(card instanceof ReverseCard){
               return true;
            
            }else if(card instanceof SkipCard){
                SkipCard skipCard = ((SkipCard)card);
                if(this.getColor() == skipCard.getColor() || this.getColor() == 'w'){return true;}
                else{return false;}
            
            }else{
                return true;
            }

    }
    

}
