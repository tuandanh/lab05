public class PickUp2Card extends ColorCards {

    //constructor
    public PickUp2Card(char color){
        super(color);
    }

    public boolean canPlay(Cards card){
        if(card instanceof NumberedCards){
            NumberedCards NumCard = ((NumberedCards)card);
            if(this.getColor() == NumCard.getColor() || this.getColor() == 'w') {return true;}
            else{return false;}

        }else if(card instanceof PickUp2Card){
            return true;

        }else if(card instanceof ReverseCard){
            ReverseCard ReverseCard = ((ReverseCard)card);
            if (this.getColor() == ReverseCard.getColor() || this.getColor() == 'w'){return true;}
            else{return false;}
        
        }else if(card instanceof SkipCard){
            SkipCard skipCard = ((SkipCard)card);
            if(this.getColor() == skipCard.getColor() || this.getColor() == 'w'){return true;}
            else{return false;}
        
        }else{
            return true;
        }

    }
}
