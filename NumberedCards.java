public class NumberedCards extends ColorCards {
    private int number;

    public NumberedCards(char color, int number){
        super(color);
        this.number = number;
    }


    public boolean canPlay(Cards card){
        if(card instanceof NumberedCards){
            NumberedCards NumCard = ((NumberedCards)card);
            if(this.getColor() == NumCard.getColor() || this.getNumber() == NumCard.getNumber() || this.getColor() == 'w') {return true;}
            else{return false;}

        }else if(card instanceof PickUp2Card){
            PickUp2Card CardPickUp = ((PickUp2Card)card);
            if(this.getColor() == CardPickUp.getColor() || this.getColor() == 'w') {return true;}
            else{return false;}

        }else if(card instanceof ReverseCard){
            ReverseCard ReverseCard = ((ReverseCard)card);
            if (this.getColor() == ReverseCard.getColor() || this.getColor() == 'w'){return true;}
            else{return false;}
        
        }else if(card instanceof SkipCard){
            SkipCard skipCard = ((SkipCard)card);
            if(this.getColor() == skipCard.getColor() || this.getColor() == 'w'){return true;}
            else{return false;}
        
        }else{
            return true;
        }
    }
    
    
    public int getNumber(){
        return this.number;
    }

}
