import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
public class UNOCardTest {
    @Test
    public void testWildCardWithColorCard(){
    WildCard wildCard = new WildCard();
    WildPickUp4Card wildCardPickUp = new WildPickUp4Card();
    // test 4 colors
    NumberedCards numberCardRed = new NumberedCards('r', 1);
    NumberedCards numberCardYellow = new NumberedCards('y', 1);
    NumberedCards numberCardBlue = new NumberedCards('b', 1);
    NumberedCards numberCardGreen = new NumberedCards('g', 1);
    assertTrue(wildCard.canPlay(numberCardRed));
    assertTrue(wildCard.canPlay(numberCardYellow));
    assertTrue(wildCard.canPlay(numberCardBlue));
    assertTrue(wildCard.canPlay(numberCardGreen));
        //test with pick up 4 cards
    assertTrue(wildCardPickUp.canPlay(numberCardRed));
    assertTrue(wildCardPickUp.canPlay(numberCardYellow));
    assertTrue(wildCardPickUp.canPlay(numberCardBlue));
    assertTrue(wildCardPickUp.canPlay(numberCardGreen));

    }
    @Test
    //test WildCard With NumberCard
    public void testWildCardWithNumberCard(){
        NumberedCards numberCard0 = new NumberedCards('r', 0);
        NumberedCards numberCard1 = new NumberedCards('r', 1);
        NumberedCards numberCard2 = new NumberedCards('r', 2);
        NumberedCards numberCard3 = new NumberedCards('r', 3);
        NumberedCards numberCard4 = new NumberedCards('r', 4);
        NumberedCards numberCard5 = new NumberedCards('r', 5);
        NumberedCards numberCard6 = new NumberedCards('r', 6);
        NumberedCards numberCard7 = new NumberedCards('r', 7);
        NumberedCards numberCard8 = new NumberedCards('r', 8);
        NumberedCards numberCard9 = new NumberedCards('r', 9);

        WildCard wildCard = new WildCard();
        WildPickUp4Card wildCardPickUp = new WildPickUp4Card();
        //test 10 number cards with wild card change color
        assertTrue(wildCard.canPlay(numberCard0));
        assertTrue(wildCard.canPlay(numberCard1));
        assertTrue(wildCard.canPlay(numberCard2));
        assertTrue(wildCard.canPlay(numberCard3));
        assertTrue(wildCard.canPlay(numberCard4));
        assertTrue(wildCard.canPlay(numberCard5));
        assertTrue(wildCard.canPlay(numberCard6));
        assertTrue(wildCard.canPlay(numberCard7));
        assertTrue(wildCard.canPlay(numberCard8));
        assertTrue(wildCard.canPlay(numberCard9));
        // test for pick up 4 cards
        assertTrue(wildCardPickUp.canPlay(numberCard0));
        assertTrue(wildCardPickUp.canPlay(numberCard1));
        assertTrue(wildCardPickUp.canPlay(numberCard2));
        assertTrue(wildCardPickUp.canPlay(numberCard3));
        assertTrue(wildCardPickUp.canPlay(numberCard4));
        assertTrue(wildCardPickUp.canPlay(numberCard5));
        assertTrue(wildCardPickUp.canPlay(numberCard6));
        assertTrue(wildCardPickUp.canPlay(numberCard7));
        assertTrue(wildCardPickUp.canPlay(numberCard8));
        assertTrue(wildCardPickUp.canPlay(numberCard9));
    }
    @Test
    public void testNumberCardWithNumberCardSameNumber(){
        // test with same number
    NumberedCards numberCardRed1 = new NumberedCards('r', 1);
    NumberedCards numberCardYellow1 = new NumberedCards('y', 1);
    assertTrue(numberCardRed1.canPlay(numberCardYellow1));
    NumberedCards numberCardGreen2 = new NumberedCards('g', 2);
    NumberedCards numberCardBlue2 = new NumberedCards('b', 2);
    assertTrue(numberCardGreen2.canPlay(numberCardBlue2));
    NumberedCards numberCardRed3 = new NumberedCards('r', 3);
    NumberedCards numberCardBlue5 = new NumberedCards('b', 5);
    assertFalse(numberCardRed3.canPlay(numberCardBlue5));
    }
    @Test
    public void testNumberCardWithNumberCardSameColor(){
        //test with same color
    NumberedCards numberCardRed1 = new NumberedCards('r', 1);
    NumberedCards numberCardRed2 = new NumberedCards('r', 2);
    assertTrue(numberCardRed1.canPlay(numberCardRed2));
    NumberedCards numberCardGreen2 = new NumberedCards('g', 2);
    NumberedCards numberCardGreen5 = new NumberedCards('g', 5);
    assertTrue(numberCardGreen2.canPlay(numberCardGreen5));
    NumberedCards numberCardRed3 = new NumberedCards('r', 3);
    NumberedCards numberCardBlue5 = new NumberedCards('b', 5);
    assertFalse(numberCardRed3.canPlay(numberCardBlue5));  
    }
    @Test
    //test PickUp2Card With NumberCard
    public void testPickUp2CardWithNumberCard(){
        NumberedCards numberCardRed1 = new NumberedCards('r', 1);
        PickUp2Card CardPickUpRed = new PickUp2Card('r');
        assertTrue(numberCardRed1.canPlay(CardPickUpRed));
        assertTrue(CardPickUpRed.canPlay(numberCardRed1));

        NumberedCards numberCardYellow3 = new NumberedCards('y', 3);
        PickUp2Card CardPickUpGreen = new PickUp2Card('g');
        assertFalse(numberCardYellow3.canPlay(CardPickUpGreen));
        assertFalse(CardPickUpGreen.canPlay(numberCardYellow3));
    }
    @Test
    //test SkipCard With NumberCard
    public void testSkipCardWithNumberCard(){
        NumberedCards numberCardRed1 = new NumberedCards('r', 1);
        SkipCard skipCardRed = new SkipCard('r');
        assertTrue(numberCardRed1.canPlay(skipCardRed));
        assertTrue(skipCardRed.canPlay(numberCardRed1));

        NumberedCards numberCardYellow3 = new NumberedCards('y', 3);
        SkipCard skipCardGreen = new SkipCard('g');
        assertFalse(numberCardYellow3.canPlay(skipCardGreen));
        assertFalse(skipCardGreen.canPlay(numberCardYellow3));
    }
    @Test
    // test ReverseCard With NumberCard
    public void testReverseCardWithNumberCard(){
        NumberedCards numberCardRed1 = new NumberedCards('r', 1);
        ReverseCard reverseCardRed = new ReverseCard('r');
        assertTrue(numberCardRed1.canPlay(reverseCardRed));
        assertTrue(reverseCardRed.canPlay(numberCardRed1));

        NumberedCards numberCardYellow3 = new NumberedCards('y', 3);
        ReverseCard reverseCardGreen = new ReverseCard('g');
        assertFalse(numberCardYellow3.canPlay(reverseCardGreen));
        assertFalse(reverseCardGreen.canPlay(numberCardYellow3));
    }
    @Test
    //test SpeicalCard With WildCard
    public void testSpeicalCardWithWildCard(){
        ReverseCard reverseCardRed = new ReverseCard('r');
        SkipCard skipCardGreen = new SkipCard('g');
        PickUp2Card CardPickUpYellow = new PickUp2Card('y');
        WildCard wildCard = new WildCard();
        assertTrue(wildCard.canPlay(CardPickUpYellow));
        assertTrue(wildCard.canPlay(skipCardGreen));
        assertTrue(wildCard.canPlay(reverseCardRed));

        assertTrue(CardPickUpYellow.canPlay(wildCard));
        assertTrue(skipCardGreen.canPlay(wildCard));
        assertTrue(reverseCardRed.canPlay(wildCard));

        WildPickUp4Card wildCardPickUp = new WildPickUp4Card();    
        assertTrue(wildCardPickUp.canPlay(CardPickUpYellow));
        assertTrue(wildCardPickUp.canPlay(skipCardGreen));
        assertTrue(wildCardPickUp.canPlay(reverseCardRed));

        assertTrue(CardPickUpYellow.canPlay(wildCardPickUp));
        assertTrue(skipCardGreen.canPlay(wildCardPickUp));
        assertTrue(reverseCardRed.canPlay(wildCardPickUp));
    }
    @Test
    // test the same value same color card
    public void testSameValueAndColorCard(){
        ReverseCard reverseCardRed = new ReverseCard('r');
        assertTrue(reverseCardRed.canPlay(reverseCardRed));
        NumberedCards numberCardGreen2 = new NumberedCards('g', 2);
        assertTrue(numberCardGreen2.canPlay(numberCardGreen2));

    }
    @Test
    //test special card with special Card
    public void testSpecialCardWithSpecialCard(){
        ReverseCard reverseCardRed = new ReverseCard('r');
        SkipCard skipCardRed = new SkipCard('r');
        PickUp2Card CardPickUpRed = new PickUp2Card('r');
        assertTrue(reverseCardRed.canPlay(skipCardRed));
        assertTrue(reverseCardRed.canPlay(CardPickUpRed));

        assertTrue(skipCardRed.canPlay(reverseCardRed));
        assertTrue(skipCardRed.canPlay(CardPickUpRed));

        assertTrue(CardPickUpRed.canPlay(reverseCardRed));
        assertTrue(CardPickUpRed.canPlay(skipCardRed));
    }
}