public class SkipCard extends ColorCards{
    
    //constructor
    public SkipCard(char color){
        super(color);
    }

    public boolean canPlay(Cards card){

        if(card instanceof NumberedCards){
            NumberedCards NumCard = ((NumberedCards)card);
            if(this.getColor() == NumCard.getColor() || this.getColor() == 'w') {return true;}
            else{return false;}

        }else if(card instanceof PickUp2Card){
            PickUp2Card CardPickUp = ((PickUp2Card)card);
            if(this.getColor() == CardPickUp.getColor() || this.getColor() == 'w') {return true;}
            else{return false;}

        }else if(card instanceof ReverseCard){
            ReverseCard ReverseCard = ((ReverseCard)card);
            if (this.getColor() == ReverseCard.getColor() || this.getColor() == 'w'){return true;}
            else{return false;}
        
        }else if(card instanceof SkipCard){
            return true;
        }else{
            return true;
        }
    }
    

}
