import java.util.Random;
import java.util.ArrayList;
public class Deck {
    private ArrayList<Cards> cards;
    private Random rd;
    private final int NUMBER_OF_WILDCARD = 4;
    private final int NUMBER_OF_NUMBEREDCARD = 9;
    private final int NUMBER_OF_COLOR = 4;
    private final int NUMBER_OF_SPECIAL_CARD = 3;
    private final int NUMBER_OF_UNO_CARDS = 112;

    public Deck(){
        this.cards = new ArrayList<Cards>();
        this.rd = new Random();
        createDeck();
        shuffle();
    }

    private void createDeck(){
        // adding normal cards
        char color = 'r';
        for(int i = 0; i < NUMBER_OF_COLOR; i++){
            if(i == 1){color = 'y';}
            if(i == 2){color = 'g';}
            if(i == 3){color = 'b';}
            // adding the special card
            for(int a = 0; a < NUMBER_OF_SPECIAL_CARD; a++){
                // add pair of "pick up 2 cards" with different color ---> total 8 cards for 4 times loop
                Cards pickUp2Card = new PickUp2Card(color);
                    cards.add(pickUp2Card);
                    cards.add(pickUp2Card);
                // add pair of "reverse card" with different color ---> total 8 cards for 4 times loop
                Cards CardReverse = new ReverseCard(color);
                    cards.add(CardReverse);
                    cards.add(CardReverse);
                // add pair of "Skip Card" with different color ---> total 8 cards for 4 times loop
                Cards skipCard = new SkipCard(color);
                    cards.add(skipCard);
                    cards.add(skipCard);
            }
            // adding the 10 number cards --> total 80 cards for 4 times color loop
            for(int j = 0; j <= NUMBER_OF_NUMBEREDCARD; j++){
                Cards card = new NumberedCards(color,j);
                    cards.add(card);
                    cards.add(card);
            }
        }

        // adding 8 wild cards
        for(int i = 0; i < NUMBER_OF_WILDCARD; i++){
            Cards card = new WildCard();
            cards.add(card);
            Cards PickUp4card = new WildPickUp4Card();
            cards.add(PickUp4card);
        }
    }

    public void addToDeck(Cards card){
        this.cards.add(card);
        
    }

    public Cards draw(){
        Cards card = cards.remove(0);
        return card;
    }

    public void shuffle(){
    int index;
        for(int i = 0; i < NUMBER_OF_UNO_CARDS; i++){
            index = rd.nextInt(113);
            // array have 1 2 3 and suppose index = 3
            Cards holder = cards.get(i); // holder hold 1 
            cards.set(i,cards.get(index)); // set "position 1" 1 to 3 => array now is 3 2 3 
            cards.set(index,holder); // set "position 3" back to (holder 1) => array now is 3 2 1
        }
    }
}
