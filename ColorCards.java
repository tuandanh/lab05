public abstract class ColorCards implements Cards {
    protected char color;
//constructor
    public ColorCards(char color){
        this.color = color;
    }

    public abstract boolean canPlay(Cards card);

    public char getColor(){
        return this.color;
    }

    

}
